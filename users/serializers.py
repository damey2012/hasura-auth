from rest_framework import serializers
from . import models

class UserSerializer(serializers.ModelSerializer):
    # x-hasura-user-id = serializers.CharField(source='id')
    class Meta:
        model = models.CustomUser
        fields = ('email', 'username',)

